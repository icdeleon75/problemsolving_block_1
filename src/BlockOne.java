import java.security.spec.RSAOtherPrimeInfo;
import java.util.Scanner;

public class BlockOne {

    public static void main(String[] args){
        itEndsInFour(544);
        hasThreeDigits(-888);
        digitsSum(86);
        digitsArePair(88);
        itIsPrime((short) 8);
        isPrimeAndNegative(-8);
        areItsDigitsPrime(11);
        areMultiple(24);
        areEqual(-88);
        isBiggest(7,7);
        commonDigits(87,98);
        sumIsPair(-89,5);
        totalDigitsSum(11,12);
        threeDigitsSum(111);
        twoOfThreeDigitsAreEqual(343);
        biggestOfThree(734);
        threeDigitsMultiple(639);
       // biggestOfThree();
        inAscOrder(9,-4,3);
        biggestDigitOfThreeNumbers(444,555,666);
        firstAndLastAreEqual(464);
        primeDigitsItHas(187);
        howManyPairHas(489);
        equalSumOfOthers(844);
        fourDigitsSum(2345);
        howManyPairItHas(2345);
        integerPositiveAndPrime(8);
        isCapicuo(12321);
        secondAndPenultimateAreEqual(4536);
        isEqualToTen(86);
        isMultipleOfSeven(48);
        EndsInSeven(67);
        howManyDigits(768);
        printTwoDigits(68);
        pairOrUnpair(6665);
        isMultipleOf(7,49);
        lastDigitAreEqual(345,565, 875);
        penultimateDigitAreEqual(456,858, -788658);
        differenceBetweenTwo(17,3);
        differenceIsPrime(56,15);
        differenceIsPair(37,5);
        exactDivisor(15,4);
        firstIsMultiple(5669);
        smallerAndPrime(564);
        lastDigitIsPrime(47);
        exerciseFortyFive(7);
        exerciseFortySix(61);
        exerciseFifty(36);
        exerciseFortySeven(48,2);
    }

    // 1 - read an integer and determine if it is a number ending in 4
    static void itEndsInFour(int num){
        if(num % 10 == 4)
            System.out.println("The number ending in 4");
        else
            System.out.println("The number don't ending in 4");
    }

    // 2 - Read an integer and determine if it has 3 digits
    static void hasThreeDigits(int num){
        if(num <0){
            num = num * -1;
        }
        if(num > 99 && num <1000)
            System.out.println("The number has three digits");
        else
            System.out.println("The number don't have three digits");
    }

    // 3 - read an integer and determine if it is negative
    static void isNegative(int num){
        if(num <0)
            System.out.println("The number is negative");
        else
            System.out.println("The number is positive");
    }

    // 4 - Read a two-digit integer and determine what the sum of its digits is equal to.
    static void digitsSum(int num){
        if(num < 0)
            num = num * -1;
        if(num > 9 && num < 100){
            System.out.println("the sum of "+num +" digits is equal to  "+((num / 10) + (num % 10)));
        }else
            System.out.println("The number must have two digits");
    }

    // 5 - Read a two-digit integer and determine if its digits are pair.
    static void digitsArePair(int num){
        if(num < 0)
            num = num * -1;
        if(num > 9 && num < 100){
            if( ((num / 10) % 2 ==0) && ((num % 10) % 2 ==0))
                System.out.println("The digits of "+num+" are pair");
            else
                System.out.println("The digits of "+num+" aren't pair");
        }else
            System.out.println("The number must have two digits");
    }

    // 6 - read a two-digit integer less than 20 and determine if it is a prime
    static void itIsPrime(short num){
        if(num < 0 || num > 20) {
            System.out.println("Introduce a number smaller than 20 and positive");
            return;
        }
        short counter=0;
        for(int i=1; i<=num;i++){
            if(num % i == 0)
                counter++;
        }
        if(counter <= 2)
            System.out.println("The number is prime");
        else
            System.out.println("The number isn't prime");
    }

    // 7 - read a two-digits integer and determine if its prime and if is negative
    static void isPrimeAndNegative(int num){
        boolean isNegative=false;
        if(num < 0){
            num =  num * -1;
            isNegative = true;
        }
        short counter=0;
        for(int i=1; i<=num;i++){
            if(num % i == 0)
                counter++;
        }
        if(counter <= 2) {
            if(isNegative){
                System.out.println("the number is prime and negative");
            }
            else{
                System.out.println("the number is prime and positive");
            }
        }else{
            if(isNegative){
                System.out.println("the number isn't prime and negative");
            }
            else{
                System.out.println("the number isn't prime and positive");
            }
        }
    }

    // 8 - Read a two digits integer and determine if its digits are prime
    static void areItsDigitsPrime(int num){
        if(num < 0 )
            num = num * -1;

        short a = (short)(num / 10);
        short b = (short)(num %10);
        short counter_a = 0, counter_b =0;

        for(int i=1;i <= a;i++){
           if(a % i ==0)
               counter_a++;
        }
        for (int i =1;i <= b;i++){
            if(b % i ==0)
                counter_b++;
        }

        if(counter_a <= 2 && counter_b <=2)
            System.out.println("The digits "+a+" and "+b+" are prime");
        else
            System.out.println("one or none of the digits are not prime");
    }

    // 9 - Read a two-digit number and determinate if a digit is multiple of the other
    static void areMultiple(int num){
        if(num <0)
            num = num * -1;

        if((num / 10) % (num % 10) == 0)
            System.out.println("The digit " + (num / 10)+" is multiple of "+(num % 10));
        else if ((num % 10) % (num / 10) == 0)
            System.out.println("The digit " + (num % 10)+" is multiple of "+(num / 10));
        else
            System.out.println("There are not any multiple");
    }

    // 10 - Read a two-digit number and determinate if its digits are equal.
    static void areEqual(int num){
        if((num/10) == (num%10))
            System.out.println("The digits are equal");
        else
            System.out.println("The digits aren't equal");
    }

    // 11 - Read two integers and determine which one is the biggest
    static void isBiggest(int one, int two){
                if(one > two)
            System.out.println("The number "+one +" is the biggest");
        else
            System.out.println("The number "+two +" is the biggest");
        if(one == two)
            System.out.println("The numbers are equal");
    }

    // 12 - Read two two-digits integers and determine if they have common digits
    static void commonDigits(int numOne, int numTwo){
        if((numOne <= 9 || numOne >= 100) || (numTwo <= 9 || numTwo >= 100)){
            System.out.println("The numbers must be two-digits integers");
            return;
        }
        if(((numOne / 10) == (numTwo / 10)) || ((numOne % 10) == (numTwo / 10))
                || ((numOne / 10) == (numTwo % 10)) || ((numOne % 10) == (numTwo % 10)))
            System.out.println("The numbers have digits in common");
        else
            System.out.println("The numbers don't have digits in common");
    }

    // 13 - Read two two-digits integers and determine if its sum generate a pair number
    static void sumIsPair(int numOne, int numTwo){
        if((numOne < 10 && numOne > 99) || (numTwo < 10 && numTwo > 9)) {
            System.out.println("the numbers must be two-digits integers");
            return;
        }
        if((numOne + numTwo) % 2 ==0)
            System.out.println("The sum of the provided digits generate a pair number: "+ (numOne + numTwo));
        else
            System.out.println("The sum of the provided digits don't generate a pair number: "+ (numOne + numTwo));
    }

    // 14 - Read two two-digits integers and determine how much is the sum of all the digits equal
    static void totalDigitsSum(int numOne, int numTwo){
        if((numOne < 10 && numOne > 99) || (numTwo < 10 && numTwo > 9)) {
            System.out.println("the numbers must be two-digits integers");
            return;
        }
        System.out.println("The sum of all the digits is equal to: "+ ((numOne % 10) + (numOne / 10) + (numTwo % 10) + (numTwo / 10)));
    }

    // 15 - Read a three-digits integer and determine how much is its digits equal to
    static void threeDigitsSum(int num){
        if(num > 999 || num < 100){
            System.out.println("The number must have three digits");
            return;
        }
        System.out.println("The sum of the digits of "+num+" is equal to: " +((num / 100) + ((num /10) % 10) + (num % 10)));
    }

    // 16 - Read a three digits integer and determine if two of its digits are equal
    static void twoOfThreeDigitsAreEqual(int num){
        if(num > 999 || num < 100){
            System.out.println("The number must have three digits");
            return;
        }
        int firstDigit = (num / 100), secondDigit = ((num /10) % 10), thirdDigit = (num % 10);
        if(firstDigit == secondDigit || firstDigit == thirdDigit || thirdDigit == secondDigit)
            System.out.println("The number have two equal digits at least");
        else
            System.out.println("The number don't have equal digits");
    }

    // 17 - Read a three-digits integer and determine in which position is the biggest
    static void biggestOfThree(int num){
        if(num > 999 || num < 100){
            System.out.println("The number must have three digits");
            return;
        }
        int firstDigit = (num / 100), secondDigit = ((num /10) % 10), thirdDigit = (num % 10);
        if(firstDigit > secondDigit && firstDigit > thirdDigit){
            System.out.println("The biggest digit is at first position and is: "+ firstDigit);
        } else if (secondDigit > firstDigit && secondDigit > thirdDigit){
            System.out.println("The biggest digit is at second position and is: "+ secondDigit);
        } else
            System.out.println("The biggest digit is at third position and is: "+thirdDigit);
    }

    // 18 - Read a three-digits integer and determine if a digits is multiple for the others
    static void threeDigitsMultiple(int num){
        if(num > 999 || num < 100){
            System.out.println("The number must have three digits");
            return;
        }
        int firstDigit = (num / 100), secondDigit = ((num /10) % 10), thirdDigit = (num % 10);
        if(secondDigit % firstDigit ==0 && thirdDigit %firstDigit ==0){
            System.out.println("The digit "+firstDigit+" is multiple of the others");
        }else if (firstDigit % secondDigit == 0 && thirdDigit % secondDigit ==0){
            System.out.println("The digit "+secondDigit+" is multiple of the others");
        }else if(firstDigit % thirdDigit == 0 && secondDigit % thirdDigit ==0){
            System.out.println("The digit "+thirdDigit+" is multiple of the others");
        }else {
            System.out.println("There is not a digit multiple of the other two");
        }
    }

    // 19 - Read three integers and determine which is biggest. Use just two variables for the integers.
    static void biggestOfThree(){
        Scanner scanner = new Scanner(System.in);
        int numOne=0, numTwo=0;
        System.out.println("Introduce the first integer");
        numOne = scanner.nextInt();
        System.out.println("Introduce the second integer");
        numTwo = scanner.nextInt();
        System.out.println("Introduce the third integer");
        if(numOne > numTwo)
            numTwo = scanner.nextInt();
        else if (numOne < numTwo)
            numOne = scanner.nextInt();
        else
            numOne = scanner.nextInt();

        if(numOne > numTwo)
            System.out.println("The biggest is: " +numOne);
        else if (numOne < numTwo)
            System.out.println("The biggest is: " +numTwo);
        else
            System.out.println("The number are equal");
    }

    // 20 - Read three integer and show then in asc order
    static void inAscOrder(int numOne, int numTwo, int numThree){
        // NOTE: This first block of exercises don't provide information to use arrays.
        if(numOne >= numTwo && numOne >= numThree){
            if(numTwo > numThree || numTwo == numThree){
                System.out.println(numThree+"\n"+numTwo+"\n"+numOne);
                return;
            }else{
                System.out.println(numTwo+"\n"+numThree+"\n"+numOne);
                return;
            }
        }
        if(numTwo >= numOne && numTwo >= numThree){
            if (numOne > numThree || numOne == numThree){
                System.out.println(numThree +"\n"+numOne+"\n"+numTwo);
                return;
            }else{
                System.out.println(numOne +"\n"+numThree+"\n"+numTwo);
            return;
            }
        }
        if(numThree >= numOne && numThree >= numTwo){
            if(numOne > numTwo || numOne == numTwo){
                System.out.println(numTwo+"\n"+numOne+"\n"+numThree);
                return;
            }else {
                System.out.println(numOne + "\n" + numTwo + "\n" + numThree);
                return;
            }
        }
        if(numOne == numThree && numOne == numTwo)
            System.out.println("All the digits are equal");
    }

    // 21 - Read three three-digits integers and determine which one have the biggest digit
    static void biggestDigitOfThreeNumbers(int numOne, int numTwo, int numThree){
        // NOTE: This first block of exercises don't provide information to use arrays.
        if((numOne <=99 || numOne > 999) || (numTwo <=99 || numTwo > 999) || (numThree <=99 || numThree > 999)){
            System.out.println("All the digits provided must be three-digits integers");
        }
        int biggestOfOne =0,biggestOfOTwo=0,biggestOfThree=0;
        // get the biggest of first number
        if((numOne / 100) >= ((numOne / 10) % 10) && (numOne / 100) >= (numOne % 10)){
             biggestOfOne = (numOne / 100);
        }else if(((numOne / 10) % 10) >= (numOne / 100) && ((numOne / 10) % 10) >= (numOne % 10)){
            biggestOfOne = (numOne / 10) % 10;
        }else if ((numOne %10) >= (numOne / 100) && (numOne % 10) >= ((numOne / 10) % 10)){
            biggestOfOne = (numOne % 10);
        }
        // get the biggest of second number
        if((numTwo / 100) >= ((numTwo / 10) % 10) && (numTwo / 100) >= (numTwo % 10)){
             biggestOfOTwo = (numTwo / 100);
        }else if(((numTwo / 10) % 10) >= (numTwo / 100) && ((numTwo / 10) % 10) >= (numTwo % 10)){
             biggestOfOTwo = (numTwo / 10) % 10;
        }else if ((numTwo %10) >= (numTwo / 100) && (numTwo % 10) >= ((numTwo / 10) % 10)){
             biggestOfOTwo = (numTwo % 10);
        }
        // get the biggest of third number
        if((numThree / 100) >= ((numThree / 10) % 10) && (numThree / 100) >= (numThree % 10)){
             biggestOfThree = (numThree / 100);
        }else if(((numThree / 10) % 10) >= (numThree / 100) && ((numThree / 10) % 10) >= (numThree % 10)){
            biggestOfThree = (numThree / 10) % 10;
        }else if ((numThree %10) >= (numThree / 100) && (numThree % 10) >= ((numThree / 10) % 10)){
            biggestOfThree = (numThree % 10);
        }

        if (biggestOfOne > biggestOfOTwo && biggestOfOne > biggestOfThree)
            System.out.println("The biggest digit is on the number: "+numOne);
        else if(biggestOfOTwo > biggestOfOne && biggestOfOTwo > biggestOfThree)
            System.out.println("The biggest digit is in the number "+numTwo);
        else if (biggestOfThree > biggestOfOne && biggestOfThree > biggestOfOTwo)
            System.out.println("The biggest digit is in the number "+numThree);
        else
            System.out.println("The biggest digit is in more than one of the provided integers");
    }

    // 22 - Read a three digits integer and determine if the first and last digits are equal
    static void firstAndLastAreEqual(int num){
        if(num <=99 && num >999)
            System.out.println("The provide number must be a three-digits integer");

        if((num / 100) == (num % 10))
            System.out.println("The first and last digits are equal");
        else
            System.out.println("the first and last digits are not equal");
    }

    // 23 - Read a three-digits integer and determine how many prime digits has.
    static void primeDigitsItHas(int num){
        if(num < 100 || num > 999){
            System.out.println("The number must be a three digits integer");
            return;
        }
        int counterOne,primeCounter=0,copy=num;
        while (num != 0){
            counterOne=0;
            for(int i=1;i <= (num % 10);i++){
                if((num % 10) % i == 0)
                    counterOne++;
            }
            if(counterOne == 2 || counterOne == 1)
                primeCounter++;
            num = num / 10;
        }
        System.out.println("The number "+ copy+" has "+primeCounter+" prime digits");
    }

    // 24 - Read a three-digits integer and determine how many pair digits has.
    static void howManyPairHas(int num){
        if(num < 100 || num > 999){
            System.out.println("The number must be a three digits integer");
            return;
        }
        int pairCounter=0,copy =num;
        while (num != 0){
            if((num % 10) % 2 ==0)
                pairCounter++;
            num = num / 10;
        }
        System.out.println("The number "+copy+" has "+pairCounter+" pair digits");
    }

    // 25 Read a three-digits integer and determine if a digits is equal to the sum of the others.
    static void equalSumOfOthers(int num){
        if(num < 100 || num > 999){
            System.out.println("The number must be a three digits integer");
            return;
        }
        int firstN = num /100;
        int secondN = (num / 10) % 10;
        int thirdN = num % 10;

        if(firstN == (secondN + thirdN) ||
        secondN == (firstN + thirdN) ||
        thirdN == (firstN +secondN)){
            System.out.println("One of the digits is equal to the sum of the others two");
        }else {
            System.out.println("Any number is equal to the sum of the others");
        }
    }

    // 26 - Read a four-digits integer and determine how many is its digits sum equal to.
    static void fourDigitsSum(int num){
        if(num < 1000 || num > 9999){
            System.out.println("The number must be a four-digits integer");
        }
        int digitsSum=0;
        while (num !=0){
          digitsSum = digitsSum + (num % 10);
          num = (num / 10);
        }
        System.out.println("The sum of the digits is equal to "+ digitsSum);
    }

    // 27 - Read a four-digits integer and determine how many pair digits it has
    static void howManyPairItHas(int num){
        if(num < 1000 || num > 9999){
            System.out.println("The number must be a four-digits integer");
        }
        int pairCounter=0, copy = num;
        while (num !=0){
            if((num % 10) % 2 ==0)
                pairCounter++;
            num = num / 10;
        }
        System.out.println("The number "+copy+" has "+pairCounter+" pair digits");
    }

    // 28 - Read an integer smaller than 50 positive and determine if it is prime
    static  void integerPositiveAndPrime(int num){
        if(num < 0){
            System.out.println("The number must be an integer positive and smaller than 50");
            return;
        }
        int primeCounter=0;
        for(int i=1;i <= num;i++){
            if(num % i ==0)
                primeCounter++;
        }
        if(primeCounter ==1 || primeCounter ==2){
            System.out.println("The number "+num+" is prime");
        }else
            System.out.println("The number isn't "+num+" prime");
    }

    // 29 - Read a five-digits integer and determine if is a capicuo number. Ej: 15651, 96469.
    static void isCapicuo(int num){
        if(num < 10000 || num > 99999){
            System.out.println("The provide number must be a five digits integer");
            return;
        }

        if(num < 0)
            num = num * -1;

       int firstTwo = num / 1000;
       int lastTwo = num % 100;

       if((firstTwo / 10) == (lastTwo % 10) && (firstTwo % 10) == (lastTwo / 10))
            System.out.println("The number "+num+" is capicuo");
       else
           System.out.println("The number "+num+" is not capicuo");
    }

    // 30 - Read a for digits-integer and determine if the second digits is equal to the penultimate.
    static void secondAndPenultimateAreEqual(int num){
        if(num < 1000 || num > 9999){
            System.out.println("The provide number must be a four digits integer");
            return;
        }
        if((num / 100) % 10 == (num % 100) /10)
            System.out.println("The second and penultimate digits of "+num+" are equal");
        else
            System.out.println("The second and penultimate digits of "+num+" aren't equal");
    }

    // 31 - Read an integer and determine if is equal to 10.
    static void isEqualToTen(int num){
        if(num != 10)
            System.out.println("The number is not equal to ten");
        else
            System.out.println("The number is equal to ten");
    }

   // 32 - Read an integer and determine if is multiple of 7.
    static void isMultipleOfSeven(int num){
        if(num % 7 ==0)
            System.out.println("The number is multiple of seven");
        else
            System.out.println("The number is not multiple of seven.");
    }

    // 33 - Read an integer and determine if it ends in 7.
    static void EndsInSeven(int num){
        if(num % 10 == 7)
            System.out.println("The provide number ends in 7");
        else
            System.out.println("The number don't end in seven");
    }

    // 34 - Read an integer smaller than 1000 and determine how many digits it has.
    static void howManyDigits(int num){
        if(num >= 1000){
            System.out.println("The provide number must be smaller than 1000");
            return;
        }
        int counter=0, copy = num;
        while (num != 0){
            num = num / 10;
            counter++;
        }
        System.out.println("The number "+copy+" has "+counter+" digits");
    }

    // 35 - Read a two-digits integer, save each digit in a variable and show they in the screen.
    static void printTwoDigits(int num){
        if(num > 99 || num < 10){
            System.out.println("The number must be a two-digits integer");
            return;
        }
        int a=(num /10), b = (num % 10);
        System.out.println(a+"\n"+b);
    }

    // 36 - Read a four-digits integer and determine if have more pair or unpair digits.
    static void pairOrUnpair(int num){
        if(num < 1000 || num > 9999){
            System.out.println("The number must be a four-digits integer");
            return;
        }
        int pairs=0,unpairs=0,current;
        while (num != 0){
            current = num % 10;
            if(current % 2 == 0)
                pairs++;
            else
                unpairs++;
            num = num / 10;
        }

        if(pairs > unpairs)
            System.out.println("The number have more pairs digits than unpairs");
        else if(pairs == unpairs)
            System.out.println("The number have same number of pair and unpair digits");
        else
            System.out.println("The number have more unpair digits than pairs");
    }

    // 37 - Read two integers and determine which is a multiple of which.
    static void isMultipleOf(int numOne, int numTwo){
        if(numOne % numTwo == 0)
            System.out.println("The number "+numOne+" is multiple of "+numTwo);
        else if(numTwo % numOne == 0)
            System.out.println("The number "+numTwo+" is multiple of "+numOne);
        else
            System.out.println("Any number is multiple of the other");
    }

    // 38 - Read three integers and determine if the last digits of every one are equal.
    static void lastDigitAreEqual(int numOne, int numTwo, int numThree){
        if(numOne % 10 == numTwo % 10 && numOne % 10 == numThree % 10)
            System.out.println("The last digit of every number are equal");
        else
            System.out.println("The last digit of every number are not equal");
    }


    // 39 - Read three numbers and determine if the penultimate digits of every one are equal
    static void penultimateDigitAreEqual(int numOne, int numTwo, int numThree){
        if(numOne > 9)
            numOne = (numOne % 100) / 10;
        if(numTwo > 9)
            numTwo = (numTwo % 100) / 10;
        if(numThree > 9)
            numThree = (numThree % 100) / 10;

        if(numOne == numThree && numOne == numTwo)
            System.out.println("The penultimate digit of every provided number are equal");
        else
            System.out.println("The penultimate digits of every provided number are not equal");
    }

    // 40 - read two integers and if the difference between the two is less
    // than or equal to 10 then display on the screen all the integers
    // between the smallest and the largest of the numbers read.
    static void differenceBetweenTwo(int numOne, int numTwo){
        if(numOne > numTwo){
            if(numOne % numTwo <= 10){
                System.out.println("The difference between the provide number is "+(numOne % numTwo));
                for(int i = numTwo;i < numOne;i++){
                    System.out.print(i+"\t");
                }
            }else
                System.out.println("The difference is bigger than 10: "+(numOne % numTwo));
        }else if(numTwo > numOne){
            if(numTwo % numOne <= 10){
                System.out.println("The difference between the provide number is "+(numTwo % numOne));
                for(int i = numOne;i < numTwo;i++){
                    System.out.print(i+"\t");
                }
            }else
                System.out.println("The difference is bigger than 10: "+(numOne % numTwo));
            }
        else
            System.out.println("The numbers are equal");
    }

    // 41 - Read two integers and determine if the difference is a prime number
    static void differenceIsPrime(int numOne, int numTwo){
       int biggest,smallest;
        if(numOne > numTwo){
            biggest=numOne;
            smallest=numTwo;
        }else if(numTwo > numOne){
            biggest=numTwo;
            smallest=numOne;
        }else {
            System.out.println("The provided numbers are equal");
            return;
        }
            int difference = biggest % smallest;
            int counter=0;
            for (int i=1;i<=difference;i++){
                if(difference % i == 0)
                    counter++;
            }
            if(counter <= 2)
                System.out.println("The difference of "+biggest+" and "+smallest+" is a prime digit: "+difference);
            else
                System.out.println("The difference of "+biggest+" and "+smallest+" is not a prime digit: "+difference);
    }

    // 42 - Read two integers and determine if the difference is a pair number
    static void differenceIsPair(int numOne, int numTwo){
        int biggest,smallest;
        if(numOne > numTwo){
            biggest=numOne;
            smallest=numTwo;
        }else if(numTwo > numOne){
            biggest=numTwo;
            smallest=numOne;
        }else {
            System.out.println("The provided numbers are equal");
            return;
        }
        if((biggest % smallest) % 2 == 0)
            System.out.println("The difference is a pair number: "+(biggest % smallest));
        else
            System.out.println("The difference is not a pair number: "+(biggest % smallest));
    }

    // 43 - Read two integers and determine if the difference is an exact divisor of any the numbers
    static void exactDivisor(int numOne, int numTwo){
        int max, min, difference;
        if(numOne>numTwo){
            max=numOne;
            min=numTwo;
        } else if(numTwo >numOne){
            max= numTwo;
            min= numOne;
        }else{
            System.out.println("The number are equal");
            return;
        }
        difference = (max % min);
        if(numOne % difference ==0)
            System.out.println("The difference is equal to "+difference+" and is an exact divisor of "+ numOne);
        else if(numTwo % difference ==0)
            System.out.println("The difference is equal to "+difference+" and is an exact divisor of "+ numOne);
        else
            System.out.println("The difference is equal to "+difference+" and is not an exact divisor for any provided number");
    }

    // 44 -  Read a four-digit integer and determine if the first digit is multiple of any other.
    static void firstIsMultiple(int num){
        if(num < 999 || num > 9999){
            System.out.println("The number must be a four-digits integer");
            return;
        }
        int first = num / 1000;
        int second = (num / 100) % 10;
        int third = (num % 100) / 10;
        int forth = num % 10;

        if(second % first == 0 ||
        third % first ==0 ||
        forth % first ==0)
            System.out.println("The number "+first+" is multiple of one or more digits of "+num);
        else
            System.out.println("The number "+first+" is not multiple of one or more digits of "+num);
    }

    /* 45 - Read a two-digits integer and if is pair show the sum of its digits, if is prime and smaller than 10
            show its last digit and if is multiple of 5 and smaller than 30 show its first digit.
    */

    // NOTE: this exercise is illogical. The
    static void exerciseFortyFive(int num){
        if(num < 10 || num > 99){
            if(num % 2 ==0){
                System.out.println("The sum of the digits of "+num+" is equal to: " +((num / 10)+(num % 10)));
                if(num % 5 ==0 && num < 30){
                    System.out.println("The first digit of "+num+" is "+(num / 10));
                }
            }
        }
        int counter=0;
        for(int i=1;i<=num;i++){
            if(num % i ==0)
                counter++;
        }
        if(counter <=2 && num <10)
            System.out.println("The last digit of "+num+" is equal to: "+(num % 10));
    }

    /* 46 - Read a two-digits integer. If it ends in "1" show its first digit, if it ends in "2"
    *       show the sum of its digits and if its end in "3" show the product of its digits.
    */
    static void exerciseFortySix(int num){
        if(num < 10 || num > 99){
            System.out.println("The provide number must be a four digits integer");
            return;
        }
        int lastDigit = num % 10;

        switch (lastDigit) {
            case 1 -> System.out.println("The first digit of " + num + " is " + (num / 10));
            case 2 -> System.out.println("The sum of the digits of " + num + " is: " + ((num / 10) + (num % 10)));
            case 3 -> System.out.println("The product of the digits of " + num + " is: " + ((num / 10) * (num % 10)));
            default -> System.out.println("The number don't end in 1, 2 or 3: " + num);
        }
    }

    /* 47 - Read two integers and if the difference is pair show the sum of the digits of every num
    *       if the difference is a prime number smaller than 10 then show the product of the numbers
    *       and if the difference finish with "4" show every digit in the screen*/
    static void exerciseFortySeven(int numOne, int numTwo){
        int biggest, smaller;
        if(numOne > numTwo) {
            biggest = numOne;
            smaller = numTwo;
        } else if(numTwo > numOne){
            biggest = numTwo;
            smaller = numOne;
        }else{
            System.out.println("The numbers are equal");
            return;
        }

        int difference = biggest % smaller;

        if(difference % 2 ==0){
            int sumOfBiggest=0, sumOfSmaller=0;
          while(biggest !=0){
              sumOfBiggest = sumOfBiggest + (biggest % 10);
              biggest = biggest / 10;
          }
          while(smaller != 0){
              sumOfSmaller = sumOfSmaller + (smaller % 10);
              smaller = smaller / 10;
          }
          System.out.println("The sum of the digits of "+numOne+" and "+numTwo+" is equal to "+(sumOfBiggest+sumOfSmaller));
        }

        int counter=0;
        for(int i=1;i<=difference;i++){
            if(difference % i ==0){
                counter++;
            }
        }
        if(counter <=2 && difference < 10){
            System.out.println("The product of "+numOne+" and "+numTwo+" is equal to: "+(numOne * numTwo));
        }

        if(difference % 10 == 4){
            System.out.println("The digits of "+numOne+" are:");
            while(numOne !=0){
                System.out.println(numOne % 10);
                numOne = numTwo / 10;
            }
            System.out.println("The digits of "+numTwo+" are:");
            while (numTwo != 0){
                System.out.println(numTwo % 10);
                numTwo = numTwo / 10;
            }
        }
    }


    // 48 - Read an integer and if is smaller than 100 determine if is prime
    static void smallerAndPrime(int num){
        if(num < 0)
            num = num * -1;

        int counter=0;
        if(num < 100){
            for(int i=1;i <=num; i++){
                if(num % i ==0)
                    counter++;
            }
            if(counter <=2)
                System.out.println("The number is prime");
            else
                System.out.println("The  number is not prime");
        }
        else
            System.out.println("The number is bigger than 100: "+num);
    }

    // 49 - Read an integer and if is multiple of 4 determine if it last digit is prime
    static void lastDigitIsPrime(int num){
        if(num % 4 == 0){
            int counter=0;
            for (int i=1;i <=num;i++){
                if((num % 10) % i ==0){
                    counter++;
                }
            }
            if(counter <= 2)
                System.out.println("The last digit of +"+num+" is a prime number: "+(num%10));
            else
                System.out.println("The last digit of +"+num+" is not a prime number: "+(num%10));
        }else
            System.out.println("The provided number is not multiple of 4: "+num);
    }

    /* 50 -  Read an integer and if is multiple of 4 show it half, if is multiple of 5 show its square and if is
    *        multiple of 6 show its first digit
    *       assume that the number is smaller than 100*/
    static void exerciseFifty(int num){
        if(num > 100){
            System.out.println("The number must be smaller than 100");
            return;
        }

        if(num % 4 ==0){
            System.out.println("The half of "+num+" is "+(num / 2));
        }
        if(num % 5 ==0){
            boolean squareFind =false;
            int square=1;
            while(squareFind == false){
                if(square * square == num){
                    squareFind = true;
                }
                square++;
            }
            System.out.println("The square of "+num+" is equal to "+square);
        }
        if(num % 6 ==0){
                System.out.println("The first digit of "+(num / 10));
        }
    }

}